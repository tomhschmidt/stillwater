//
//  SWAudioSelectionTableViewController.swift
//  Stillwater
//
//  Created by Thomas Schmidt on 5/2/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//
import MediaPlayer
import UIKit
class SWAudioSelectionTableViewController: UITableViewController {
    
    var mediaType: MPMediaType?
    var dataSources: [SWAudioDataSource]?
    var segmentedControl: UISegmentedControl?
    
    convenience init() {
        self.init()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let segmentedControlItems: [AnyObject] = ["Audiobooks", "Podcasts"]
        self.segmentedControl = UISegmentedControl(items: segmentedControlItems)
        segmentedControl!.addTarget(self, action: #selector(SWAudioSelectionTableViewController.segmentedControlSelectionChanged), forControlEvents: UIControlEvents.ValueChanged)
        segmentedControl!.selectedSegmentIndex = 0
        segmentedControl!.sizeToFit()
        segmentedControl!.tintColor = UIColor.orangeColor()
        
        self.navigationController!.navigationBar.topItem!.title = "Earmarks"
        self.navigationItem.titleView = segmentedControl
        
        self.dataSources = []
        self.dataSources?.append(SWAudioDataSource(mediaType: MPMediaType.AudioBook))
        self.dataSources?.append(SWAudioDataSource(mediaType: MPMediaType.Podcast))
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == kSWSeguesAudioPlayerSegue) {
            let mediaItem: MPMediaItem = dataSources![segmentedControl!.selectedSegmentIndex].mediaAtIndexPath(self.tableView.indexPathForSelectedRow!)!
            ((segue.destinationViewController as! SWAudioPlayerViewController)).currentMediaItem = mediaItem
        }
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier(kSWSeguesAudioPlayerSegue, sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func segmentedControlSelectionChanged() {
        self.tableView.dataSource = dataSources![segmentedControl!.selectedSegmentIndex]
        self.tableView.reloadData()
    }
}
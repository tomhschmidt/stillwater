//
//  SWAudioPlayerViewController.swift
//  Stillwater
//
//  Created by Thomas Schmidt on 5/1/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//

import UIKit
class SWAudioPlayerViewController: UIViewController, MPMediaPickerControllerDelegate {
    
    var currentMediaItem: MPMediaItem?
    var musicPlayer: MPMusicPlayerController?
    var trackingButtons: [AnyObject]?
    
    @IBOutlet var playPauseButton: UIButton!
    @IBOutlet var reverse30Button: UIButton!
    @IBOutlet var reverse10Button: UIButton!
    @IBOutlet var forward10Button: UIButton!
    @IBOutlet var forward30Button: UIButton!
    @IBOutlet var scrubber: UISlider!
    @IBOutlet var maxTimeLabel: UILabel!
    @IBOutlet var minTimeLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    var takeNoteButton: UIButton?
    
    var scrubberUpdateTimer: NSTimer?
    var commandCenter: MPRemoteCommandCenter?
    var notesNavigationButton: UIBarButtonItem?
    var isScrubbing: Bool?
    
    convenience init() {
        self.init()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        self.musicPlayer = MPMusicPlayerController.systemMusicPlayer()
        self.musicPlayer!.nowPlayingItem = nil
        musicPlayer!.setQueueWithItemCollection(MPMediaItemCollection(items: [currentMediaItem!]))
        musicPlayer!.beginGeneratingPlaybackNotifications()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SWAudioPlayerViewController.playbackStateChanged), name: MPMusicPlayerControllerPlaybackStateDidChangeNotification, object: nil)
        self.updateUIWithCurrentMedia()
        
        self.isScrubbing = false
        self.scrubberUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(SWAudioPlayerViewController.scrubberUpdateTimerFired), userInfo: nil, repeats: true)
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        self.commandCenter = MPRemoteCommandCenter.sharedCommandCenter()
        self.commandCenter!.nextTrackCommand.enabled = true
        commandCenter!.nextTrackCommand.addTarget(self, action: #selector(SWAudioPlayerViewController.nextTrackCommandReceived))
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateUIWithCurrentMedia()
        musicPlayer!.play()
    }

    override func loadView() {
        super.loadView()
        
        self.takeNoteButton = UIButton(type: .System)
        takeNoteButton!.addTarget(self, action: #selector(SWAudioPlayerViewController.takeNoteButtonPressed), forControlEvents: .TouchUpInside)
        takeNoteButton!.setTitle("Take Note", forState: .Normal)
        takeNoteButton!.sizeToFit()
        
        var frame: CGRect = takeNoteButton!.frame
        frame.origin.x = self.view.frame.size.width / 2.0 - frame.size.width / 2.0
        frame.origin.y = self.view.frame.size.height - 200
        takeNoteButton!.frame = frame
        
        self.view!.addSubview(takeNoteButton!)
    }

    func setupUI() {
        self.notesNavigationButton = UIBarButtonItem(title: "Notes", style: .Plain, target: self, action: #selector(SWAudioPlayerViewController.notesNavigationButtonPressed))
        self.navigationItem.rightBarButtonItem = notesNavigationButton
        self.playPauseButton.titleLabel!.textColor = UIColor.blackColor()
        self.playPauseButton.titleLabel!.adjustsFontSizeToFitWidth = true
        self.trackingButtons = [reverse30Button, reverse10Button, forward10Button, forward30Button]
        
        self.titleLabel.font = UIFont.swAudioPlayerTitleFont()
        self.subtitleLabel.font = UIFont.swAudioPlayerSubtitleFont()
        
        for(var i = 0; i < trackingButtons?.count; i += 1) {
            trackingButtons![i].addTarget(self, action: #selector(SWAudioPlayerViewController.timeSkipButtonPressed(_:)), forControlEvents: .TouchUpInside)
            trackingButtons![i].titleLabel!.font = UIFont.swAudioPlayerTrackingButtonFont()
        }
        
        
        reverse30Button.setTitle("-30", forState: .Normal)
        reverse10Button.setTitle("-10", forState: .Normal)
        playPauseButton.setTitle("Play", forState: .Normal)
        forward10Button.setTitle("+10", forState: .Normal)
        forward30Button.setTitle("+30", forState: .Normal)
        playPauseButton.addTarget(self, action: #selector(SWAudioPlayerViewController.playOrPauseMusic(_:)), forControlEvents: .TouchUpInside)
        
        self.scrubber.minimumTrackTintColor = UIColor.blackColor()
        self.scrubber.maximumTrackTintColor = UIColor.blackColor()
        scrubber.addTarget(self, action: #selector(SWAudioPlayerViewController.scrubberValueChanged), forControlEvents: .TouchUpInside)
        scrubber.addTarget(self, action: #selector(SWAudioPlayerViewController.scrubberValueChanged), forControlEvents: .TouchDragExit)
        scrubber.addTarget(self, action: #selector(SWAudioPlayerViewController.scrubberValueChanging), forControlEvents: .ValueChanged)
        self.scrubber.minimumValue = 0
        self.scrubber.maximumValue = Float(currentMediaItem!.playbackDuration)
    }

    func updateUIWithCurrentMedia() {
        
        if (musicPlayer!.nowPlayingItem != nil && musicPlayer!.nowPlayingItem! != currentMediaItem) {
            musicPlayer!.setQueueWithItemCollection(MPMediaItemCollection(items: [currentMediaItem!]))
        }
        
        let playbackState: MPMusicPlaybackState = self.fixedPlaybackState()
        
        if playbackState == .Stopped || playbackState == .Paused {
            playPauseButton.setTitle("Play", forState: .Normal)
        }
        else if playbackState == .Playing {
            playPauseButton.setTitle("Pause", forState: .Normal)
        }

        self.titleLabel.text = currentMediaItem!.title
        switch currentMediaItem!.mediaType {
            case MPMediaType.AudioBook:
                self.subtitleLabel.text = "Chapter \(UInt(currentMediaItem!.albumTrackNumber))"
            case MPMediaType.Podcast:
                self.subtitleLabel.text = currentMediaItem!.podcastTitle
            default:
                break
        }

        self.updateScrubberUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func playOrPauseMusic(sender: AnyObject) {
        let playbackState: MPMusicPlaybackState = self.fixedPlaybackState()
        if playbackState == .Stopped || playbackState == .Paused {
            musicPlayer!.play()
        }
        else if playbackState == .Playing {
            musicPlayer!.pause()
        }

    }

    func playbackStateChanged() {
        self.updateUIWithCurrentMedia()
    }

    func updateScrubberUI() {
        self.minTimeLabel.text = String.stringForTimeInterval(musicPlayer!.currentPlaybackTime)
        minTimeLabel.sizeToFit()
        self.maxTimeLabel.text = String.stringForTimeInterval(currentMediaItem!.playbackDuration)
        maxTimeLabel.sizeToFit()
        self.scrubber.value = Float(musicPlayer!.currentPlaybackTime)
    }

    func scrubberValueChanged() {
        self.isScrubbing = false
        let scrubberValue: CGFloat = CGFloat(scrubber.value)
        musicPlayer!.currentPlaybackTime = Double(scrubberValue)
        self.updateScrubberUI()
    }

    func scrubberValueChanging() {
        self.isScrubbing = true
        self.minTimeLabel.text = String.stringForTimeInterval(Double(scrubber.value))
    }

    func scrubberUpdateTimerFired() {
        if !isScrubbing! {
            self.updateScrubberUI()
        }
    }

    func fixedPlaybackState() -> MPMusicPlaybackState {
        return musicPlayer!.currentPlaybackRate == 0 ? .Paused : .Playing
    }

    func timeSkipButtonPressed(sender: AnyObject) {
            // lol just shoot me
        var skipAmount: Double = 0
        if sender as! NSObject == reverse30Button {
            skipAmount = -30
        }
        else if sender as! NSObject == reverse10Button {
            skipAmount = -10
        }
        else if sender as! NSObject == forward10Button {
            skipAmount = 10
        }
        else if sender as! NSObject == forward30Button {
            skipAmount = 30
        }

        self.musicPlayer!.currentPlaybackTime += skipAmount
        self.updateUIWithCurrentMedia()
    }

    func nextTrackCommandReceived() {
        self.takeNote()
    }

    func takeNoteButtonPressed() {
        self.takeNote()
    }

    func takeNote() {
        MagicalRecord.saveWithBlock { (localContext: NSManagedObjectContext) in
            let newNote: SWNote = SWNote.MR_createEntityInContext(localContext)!
            newNote.startTime = Int(max(0, self.musicPlayer!.currentPlaybackTime - 15))
            newNote.endTime = Int(min(self.currentMediaItem!.playbackDuration, self.musicPlayer!.currentPlaybackTime + 15))
            newNote.mediaID = Int(self.currentMediaItem!.persistentID)
            newNote.dateTaken = NSDate()
        };
        
        SWAudioTranscriber.sharedInstance.transcribeMediaFile(self.currentMediaItem!, startTime: Double(max(0, self.musicPlayer!.currentPlaybackTime - 15)), endTime: Double(min(self.currentMediaItem!.playbackDuration, self.musicPlayer!.currentPlaybackTime + 15)))
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if (segue.identifier == kSWSeguesNotesViewSegue) {
            ((segue.destinationViewController as! SWNotesViewController)).mediaPersistentID = CLongLong(currentMediaItem!.persistentID)
        }
    }

    func notesNavigationButtonPressed() {
        self.performSegueWithIdentifier(kSWSeguesNotesViewSegue, sender: self)
    }
}

import MediaPlayer
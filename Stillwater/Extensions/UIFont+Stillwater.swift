//
//  UIFont+Stillwater.swift
//  Stillwater
//
//  Created by Thomas Schmidt on 5/25/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//

import Foundation
extension UIFont {
    
    class func swAudioPlayerTitleFont() -> UIFont {
        //return UIFont (name: "AkzidenzGrotesk-Regular", size: 32)!
        return UIFont.systemFontOfSize(32)
    }
    
    class func swAudioPlayerSubtitleFont() -> UIFont {
        //return UIFont (name: "AkzidenzGrotesk-Light", size: 24)!
        return UIFont.systemFontOfSize(24)
    }
    
    class func swAudioPlayerTrackingButtonFont() -> UIFont {
        //return UIFont (name: "AkzidenzGrotesk-Light", size: 18)!
        return UIFont.systemFontOfSize(18)
    }
}
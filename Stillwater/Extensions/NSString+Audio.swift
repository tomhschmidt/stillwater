//
//  NSString+Audio.swift
//  Stillwater
//
//  Created by Thomas Schmidt on 5/8/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//
import Foundation
extension NSString {

    class func stringForTimeInterval( var timeInterval: NSTimeInterval) -> String {
        if(timeInterval.isNaN) {
            timeInterval = 0
        }
        let seconds: Int = Int(timeInterval) % 60
        let minutes: Int = Int(timeInterval / 60)
        return String(format: "%d:%02d", minutes, seconds)
    }
}
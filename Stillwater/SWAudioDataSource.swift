//
//  SWAudioDataSource.h
//  Stillwater
//
//  Created by Thomas Schmidt on 5/3/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//
import MediaPlayer
import Foundation
import UIKit
class SWAudioDataSource: NSObject, UITableViewDataSource {
    
    var mediaType: MPMediaType?
    var mediaCollection: MPMediaItemCollection?

    convenience init(mediaType: MPMediaType) {
        self.init()
        self.mediaType = mediaType
        var mediaQuery: MPMediaQuery? = nil
        if mediaType == .AudioBook {
            mediaQuery = MPMediaQuery.audiobooksQuery()
        }
        else if mediaType == .Podcast {
            mediaQuery = MPMediaQuery.podcastsQuery()
        }

        self.mediaCollection = mediaQuery!.collections!.count > 0 ? mediaQuery!.collections![0] : nil
    }

    func mediaAtIndexPath(indexPath: NSIndexPath) -> MPMediaItem? {
        if indexPath.row < mediaCollection!.count {
            return mediaCollection!.items[indexPath.row]
        }
        else {
            return nil
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaCollection == nil ? 0 : mediaCollection!.count 
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kSWTableViewControllerAudioItemCell, forIndexPath: indexPath)
        cell.textLabel!.text = mediaCollection!.items[indexPath.row].title
        return cell
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
}
//
//  SWAudioDataSource.m
//  Stillwater
//
//  Created by Thomas Schmidt on 5/3/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//
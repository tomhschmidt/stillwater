//
//  SWConstants.swift
//  Stillwater
//
//  Created by Thomas Schmidt on 5/25/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//

import Foundation

// MARK: - Cell Type Names
let kSWTableViewControllerNoteCell = "noteCell"
let kSWTableViewControllerAudioItemCell = "audioSelectionCell"

// MARK: - Segues
let kSWSeguesAudioPlayerSegue = "audioPlayerSegue"
let kSWSeguesNotesViewSegue = "segueToNotes"


// MARK: - Schemas
let kSWNoteStartTime = "startTime"
let kSWNoteEndTime = "endTime"
let kSWNoteDateTaken = "dateTaken"
let kSWNoteTranscribedText = "transcribedText"
let kSWNoteMediaID = "mediaID"

// MARK: - File Names
let kSWFileNameModelFile = "SWModel"

//
//  SWNote.swift
//  
//
//  Created by Thomas Schmidt on 5/23/16.
//

import Foundation
import CoreData

public class SWNote : NSManagedObject {

    @NSManaged var startTime: NSNumber?
    @NSManaged var endTime: NSNumber?
    @NSManaged var mediaID: NSNumber?
    @NSManaged var transcribedText: String?
    @NSManaged var dateTaken: NSDate?

}

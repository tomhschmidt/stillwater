//
//  SWAudioTranscriber.h
//  Stillwater
//
//  Created by Thomas Schmidt on 5/9/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//
import Foundation
import AVFoundation
import SpeechToTextV1
import MediaPlayer

class SWAudioTranscriber: NSObject {
    
    var audioPlayer: AVPlayer?
    var speechToText: SpeechToText?
    
    static let sharedInstance = SWAudioTranscriber()

    override init() {
        speechToText = SpeechToText.init(username: "72ce5ca0-293b-42e2-8c97-52c08884cc03" , password: "2ZzuHD7sG7M4")
    }
    
    func transcriptionFailed(error: NSError) {
        let title = "Speech to Text Error:\nTranscribe"
        let message = error.localizedDescription
        NSLog("%@ %@", title, message)
    }
    
    func showResults(results: [TranscriptionResult]) {
        for result in results {
            NSLog("%@", result.final)
        }
    }
    
    func transcribeMediaFile(mediaItem: MPMediaItem, startTime: NSTimeInterval, endTime:NSTimeInterval) -> Void{
        
        let outputFileString : String = String.init(format: "%lld-%lld-%lld-temp", mediaItem.persistentID, startTime, endTime)
        let outputFileURL: NSURL = NSURL.fileURLWithPath(NSTemporaryDirectory().stringByAppendingString(outputFileString))
        
        let originalAudioFile : AVAsset = AVAsset.init(URL: mediaItem.assetURL!)
        let exportSession : AVAssetExportSession = AVAssetExportSession.init(asset: originalAudioFile, presetName: AVAssetExportPresetPassthrough)!
        
        let startCMTime : CMTime = CMTimeMake(Int64(startTime) * 100 , 100)
        let endCMTime : CMTime = CMTimeMake(Int64(endTime) * 100 , 100)
        let exportTimeRange : CMTimeRange = CMTimeRangeFromTimeToTime(startCMTime, endCMTime)
        
        exportSession.outputURL = outputFileURL
        exportSession.outputFileType = AVFileTypeWAVE
        exportSession.timeRange = exportTimeRange
        
        exportSession.exportAsynchronouslyWithCompletionHandler({
            if exportSession.status == AVAssetExportSessionStatus.Completed {
                let settings = TranscriptionSettings(contentType: AudioMediaType.WAV)
                self.speechToText!.transcribe(outputFileURL, settings: settings, failure: self.transcriptionFailed, success: self.showResults)
            } else if exportSession.status == AVAssetExportSessionStatus.Failed{
                NSLog("Export failed")
            }
        })
    }
}
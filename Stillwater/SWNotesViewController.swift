//
//  SWNotesViewController.swift
//  Stillwater
//
//  Created by Thomas Schmidt on 5/21/16.
//  Copyright © 2016 Thomas Schmidt. All rights reserved.
//

import UIKit

class SWNotesViewController: UITableViewController {
    
    var mediaPersistentID : CLongLong?
    var notes : [AnyObject]?
    override func viewDidLoad() {
        super.viewDidLoad()
        let predicate: NSPredicate = NSPredicate(format: "%@ = %lld", kSWNoteMediaID, mediaPersistentID!)
        self.notes = SWNote.MR_findAllSortedBy(kSWNoteStartTime, ascending: true, withPredicate: predicate)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kSWTableViewControllerNoteCell, forIndexPath: indexPath)
        let note: SWNote = notes![indexPath.row] as! SWNote
        cell.detailTextLabel!.text = "\(String.stringForTimeInterval(note.startTime!.doubleValue)) - \(String.stringForTimeInterval((note.endTime?.doubleValue)!))"
        if note.transcribedText != nil {
            cell.textLabel?.text = note.transcribedText
        }
        return cell
    }

}
